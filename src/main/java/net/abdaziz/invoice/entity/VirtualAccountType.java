package net.abdaziz.invoice.entity;

public enum VirtualAccountType {
    CLOSED, OPEN, INSTALLMENT
}
