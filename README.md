# Aplikasi Invoice Management #

Aplikasi ini dipakai untuk mengelola invoice dan menyambungkan dengan berbagai metode pembayaran 
Diantara metode pembayaran yang akan disupport antara lain :

* Virtual Account Bank
  * Bank BNI
  * Bank CIMB
  * Bank BSI
  

* e-Wallet
  * Ovo
  * Gopay

  Tipe tagihan yang tersedia:
    * CLOSED : bayar sesuai nominal, kalau tidak sesuai ditolak
    * OPEN : pembayaran berapapun diterima 
    * INSTALLMENT : pembayaran diterima selama total akumulasi lebih kecil atau sama dengan total nilai tagihan
    

* QR Payment
  * QRIS

# Cara setup database
1. Jalankan postgresql di docker

   ...
   docker run --rm \
   --name invoice-db \
   -e POSTGRES_DB=invoicedb \
   -e POSTGRES_USER=invoice \
   -e POSTGRES_PASSWORD=OaBH78Zhgf1VYCtpFYXZ \
   -e PGDATA=/var/lib/postgresql/data/pgdata \
   -v "$PWD/invoicedb-data:/var/lib/postgresql/data" \
   -p 5432:5432 \
   postgres:13
   ...
* Untuk menghapus folder invoicedb-data rm -rf invoicedb-data
